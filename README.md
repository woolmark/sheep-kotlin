# Sheep for Kotlin

Sheep for Kotlin is an implementation [sheep] for Kotlin.

# Requirements

 - Kotlin version 0.12.613
 - JetBrains IntelliJ and Kotlin Plugin

# License
[WTFPL] 

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[Kotlin]: https://kotlinlang.org "Kotlin"
[WTFPL]: http://www.wtfpl.net "WTFPL"

