package woolmark.sheep

import java.awt.*
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.util.*
import javax.imageio.ImageIO

val SKY_RGB_ARRAY = intArrayOf(150, 150, 255)

val GROUND_RGB_ARRAY = intArrayOf(100, 255, 100)

val MOVE_DELTA_X = 5;

val MOVE_DELTA_Y = 3;

val JUMP_FRAME = 4;

interface Area {
    var x: Int
    var y: Int
    var width: Int
    var height: Int
}

class SimpleArea(x: Int = 0, y: Int = 0, width: Int = 0, height: Int = 0) : Area {
    override var x = x
    override var y = y
    override var width = width
    override var height = height
}

class Sheep(size: Area, frame: Area, ground: Area, fence: Area) : Area {

    override var x = ground.width + size.width

    override var y= ground.y + Random().nextInt(ground.height - size.height)

    override var width = size.width

    override var height = size.height

    var stretch = false

    private var jumpCount = Int.MIN_VALUE

    private val jumpX = -1 * (y - frame.height) * fence.width / fence.height + (frame.width - fence.width) / 2

    fun run() {

        x -= MOVE_DELTA_X

        if (jumpCount < 0 && jumpX < x && x <= jumpX + MOVE_DELTA_X) {
            jumpCount = 0
        }

        when (jumpCount) {
            in 0..JUMP_FRAME-1 -> {
                y -= MOVE_DELTA_Y
                stretch = true
                jumpCount++
            }
            in JUMP_FRAME..JUMP_FRAME*2 -> {
                y += MOVE_DELTA_Y
                stretch = true
                jumpCount++
            }
            else -> {
                stretch != stretch
                jumpCount = Int.MIN_VALUE
            }
        }

    }

    fun isGoAway() : Boolean {
        return x < -width
    }

    fun isJumpTop(): Boolean {
        return jumpCount == JUMP_FRAME
    }

}

interface Woolmark {

    var frame: Area

    var fence: Area

    var ground: Area

    var sheep: Area

    var sheepAppend: Boolean

    var sheepCount: Int

    var sheepFlock: MutableList<Sheep>

    fun run() {

        if (sheepAppend) {
            sheepFlock.add(Sheep(sheep, frame, ground, fence))
        }

        var removeFlock = LinkedList<Sheep>()
        for (sheep in sheepFlock) {
            sheep.run()
            if (sheep.isJumpTop()) {
                sheepCount++
            } else if (sheep.isGoAway()) {
                removeFlock.add(sheep)
            }
        }

        sheepFlock.removeAll(removeFlock)

        if (sheepFlock.size() <= 0) {
            sheepFlock.add(Sheep(sheep, frame, ground, fence))
        }

    }

}

class SheepCanvas : Canvas(), Woolmark {

    private val SKY_RGB = Color(SKY_RGB_ARRAY[0], SKY_RGB_ARRAY[1], SKY_RGB_ARRAY[2])

    private val GROUND_RGB = Color(GROUND_RGB_ARRAY[0], GROUND_RGB_ARRAY[1], GROUND_RGB_ARRAY[2])

    private val FENCE_IMAGE = ImageIO.read(javaClass.getResource("/fence.png"))

    private val SHEEP_IMAGES = Array(2, { i ->
        when (i) {
            0 -> ImageIO.read(javaClass.getResource("/sheep00.png"))
            1 -> ImageIO.read(javaClass.getResource("/sheep01.png"))
            else -> null
        }
    })

    override var sheep: Area

    override var frame: Area

    override var fence: Area

    override var ground: Area

    override var sheepCount = 0

    override var sheepAppend = false

    override var sheepFlock: MutableList<Sheep> = LinkedList()

    private var running = true

    private var offScreenImage: Image? = null

    init {
        size = Dimension(120, 120)

        $frame = SimpleArea(width = size.width, height = size.height)

        $fence = SimpleArea(
                (size.width - FENCE_IMAGE.width) / 2, size.height - FENCE_IMAGE.height,
                FENCE_IMAGE.width, FENCE_IMAGE.height)

        val gh = (fence.height * 0.9).toInt()
        $ground = SimpleArea(0, size.height - gh, size.width, gh)

        $sheep = SimpleArea(width = SHEEP_IMAGES[0]?.width ?: 0, height = SHEEP_IMAGES[0]?.height ?: 0)

        addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent) {
                sheepAppend = true
            }
            override fun mouseReleased(e: MouseEvent) {
                sheepAppend = false
            }
        })
    }

    override fun update(g: Graphics) {
        super.update(g)

        offScreenImage?.let {
            val bg = it.graphics

            bg.color = SKY_RGB
            bg.fillRect(frame.x, frame.y, frame.width, frame.height)

            bg.color = GROUND_RGB
            bg.fillRect(ground.x, ground.y, ground.width, ground.height)

            bg.drawImage(FENCE_IMAGE, fence.x, fence.y, fence.width, fence.height, null)

            for (sheep: Sheep in sheepFlock) {
                bg.drawImage(SHEEP_IMAGES[ if (sheep.stretch) 1 else 0],
                        sheep.x, sheep.y, sheep.width, sheep.height, null)
            }

            bg.color = Color.BLACK;
            bg.drawString(sheepCount.toString() + " sheep",
                    5,
                    5 + bg.font.size);

            g.drawImage(it, 0, 0, null)
        }

    }

    fun start() {

        offScreenImage = if (offScreenImage == null) createImage(size.width, size.height) else offScreenImage

        running = true
        Thread(object : Runnable {
            override fun run() {
                while (running) {
                    this@SheepCanvas.run()
                    repaint()
                    Thread.sleep(100)
                }
            }
        }).start()

    }

    fun stop() {
        running = false
    }

}
