package woolmark.sheep

import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.JWindow
import javax.swing.SwingUtilities

fun main(args: Array<String>) {

    SwingUtilities.invokeLater(object : Runnable {
        override fun run() {

            val canvas = SheepCanvas()
            var window = JWindow()

            window.add(canvas)
            window.pack()

            val windowAdapter = object : WindowAdapter() {
                override fun windowOpened(e: WindowEvent) {
                    canvas.start()
                }
                override fun windowClosed(e: WindowEvent) {
                    canvas.stop()
                }
            }
            val mouseAdapter = object : MouseAdapter() {
                var start: MouseEvent? = null
                override fun mousePressed(me: MouseEvent) {
                    start = if (me.clickCount == 2) me else null
                }
                override fun mouseReleased(me: MouseEvent) {
                    start = null
                }
                override fun mouseDragged(me: MouseEvent) {
                    if (start != null) {
                        val eventLocationOnScreen = me.locationOnScreen
                        SwingUtilities.windowForComponent(me.component)?.setLocation(
                                eventLocationOnScreen.x - start!!.x, eventLocationOnScreen.y - start!!.y)
                    }
                }
            }

            canvas.addMouseListener(mouseAdapter)
            canvas.addMouseMotionListener(mouseAdapter)
            window.addWindowListener(windowAdapter)

            window.isLocationByPlatform = true
            window.isVisible = true
            window.toFront()

        }
    })

}